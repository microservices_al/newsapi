const bodyParser = require('body-parser');
const express = require('express');
const cors = require('cors');

const app = express();

app.use(cors())
app.use(bodyParser.json());

app.use('/api', require('./api/news.routes'));
 
app.listen(3000, () => {
    console.log("Listening on port 3000");
})
